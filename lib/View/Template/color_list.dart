import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:from_css_color/from_css_color.dart';

class ColorList{
  static final Color primaryColor = fromCssColor('#0b899b');
  static final accentColor = Colors.amber[700];

  static final headingColor = Colors.grey[800];

}