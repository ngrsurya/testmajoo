import 'package:flutter/material.dart';

Container circularProgress() {
  return Container(
    padding: EdgeInsets.all(10),
    alignment: Alignment.center,
    child: CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation(Colors.blue[800]),
    ),
  );
}

Container linearProgres() {
  return Container(
    padding: EdgeInsets.only(bottom: 10),
    child: LinearProgressIndicator(
      valueColor: AlwaysStoppedAnimation(Colors.blue[800]),
    ),
  );
}
