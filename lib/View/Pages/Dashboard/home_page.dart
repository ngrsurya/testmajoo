import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:test_majoo/Controller/home_controller.dart';
import 'package:test_majoo/Model/Movie.dart';
import 'package:test_majoo/View/Template/color_list.dart';
import 'package:test_majoo/View/Template/progress.dart';
import 'package:test_majoo/View/Widgets/item_card.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends StateMVC<HomePage> {

  
  //Controller
  HomeController _controller;
  _HomePageState(): super(HomeController())
  {
    _controller = HomeController.con;
  }


  //Status
  Future<bool> isLoading = Future<bool>.value(true);

  //Component
  TextEditingController searchTextEditing = new TextEditingController();

  //Data
  List<Widget> itemCards = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  Future<void> getData() async{
    itemCards.clear();

    print('get data');
    setLoadingValue(true);
    await _controller.getMovieLst(this.context);
    await setItemCardsData(_controller.movies);
    setLoadingValue(false);
  }


  setItemCardsData(List<Movie> _movies){
    if(_movies != null)
    {
      for(var item in _movies){
        var card =  MovieCard(func:()=> _controller?.goDetailMoviePage(this.context, item.id.toString()), itemID: item.id.toString(), year: item.year, imgURL: item.imageURl, movieName:item.title,);
        
        itemCards.add(card);
      }

      print(itemCards.length);
    }
    else
    {
      print('no data avaliable');
    }

  }

  setLoadingValue(bool val){
    setState(() {
      isLoading = Future<bool>.value(val);
    });
  }

  //===================================================================
  //BUILDER
  buildItemList(){    
    var screenSize = MediaQuery.of(context).size;
    final double itemHeight = (screenSize.height - kToolbarHeight - 50) * .45;
    final double itemWidth = screenSize.width / 2;
    return Container(
      alignment: Alignment.center,
      width: screenSize.width * .9,
      height: screenSize.height * .8,
      child: itemCards != null?
      GridView.count(
        crossAxisCount: 2,
        childAspectRatio: (itemWidth / itemHeight) ,
        mainAxisSpacing: 25,
        crossAxisSpacing: 25,
        children: itemCards,
      ) : Text('')
    );
  }

  buildAll(){
    var size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: RefreshIndicator(
        onRefresh:  ()async{
          getData();
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                buildItemList()
              ],
            )
          ),
        
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(title: Text('Home', style: TextStyle(color: Colors.white),), backgroundColor: ColorList.primaryColor,automaticallyImplyLeading: false,),
        body: FutureBuilder(
          future: isLoading,
          builder: (context, snapshot){
            if(snapshot.data == true)
              return circularProgress();
    
            return buildAll(); 
          },
        ),
      ),
    );
  }
}