import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:test_majoo/Controller/Auth/user_controller.dart';
import 'package:test_majoo/View/Template/color_list.dart';
import 'package:test_majoo/View/Template/progress.dart';

class UserPage extends StatefulWidget {
  final String message;
  final bool isSignedIn;

  UserPage({this.message, this.isSignedIn});

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends StateMVC<UserPage> {
  //Status
  Future<bool> isLoading = Future<bool>.value(false);
  bool isLogin = true;
  bool isPasswordVisible = false;

  //Text Controller
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController username = TextEditingController();

  //Controller
  UserController _controller;

  _UserPageState() : super(UserController()) {
    _controller = UserController.con;
  }

  @override
  void initState() {
    super.initState();
  }

  //================================================================================================
  //FUNCTION

  login(String email, String pass) async {
    print('Sing in');
    setLoadingValue(true);
    bool isSigned = await _controller.loginUser(this.context, email, pass);
    String message = "";
    setLoadingValue(false);
    if (!isSigned) 
    {
      message = "Kombinasi username dan password salah";
      sendSnackBar(message);
    }
  }

  createUser(String username, String email, String password) async{
    print('Sing in');
    setLoadingValue(true);
    bool status = false;
    if(username != null && username != '' && password != null && password != '' && email != null && email != '')
      status = await _controller.createUser(this.context, email, username, password);
    if(!status)
    {
      String message = "Input tidak valid";
      sendSnackBar(message);
    }
    setLoadingValue(false);
  }

  sendSnackBar(String message) {
    setState(() {
      final snackBar = SnackBar(
        content: Text(message),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  setLoadingValue(bool val){
    //Set Loading
    setState(() {
      isLoading = Future<bool>.value(val);
    });
  }

  changeIsLogingStatus(){
    setState(() {
      username.clear();
      email.clear();
      password.clear();
      isLogin = !isLogin;
    });
  }

  //================================================================================================
  buildHeader() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Selamat Datang di',
              style: TextStyle(
                  color: ColorList.accentColor,
                  fontSize: 18,
                  fontWeight: FontWeight.bold)),
          SizedBox(height: 10),
          Text('E-Tix',
              style: TextStyle(
                color: ColorList.accentColor,
                fontSize: 18,
              )),
        ],
      ),
    );
  }

  buildInputField(
      String _title, TextEditingController _txtController, bool _isPassword) {
    return Container(
      width: 200,
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(
            width: 5, color: Colors.white),
      )),
      child: TextField(
        controller: _txtController,
        obscureText: !isPasswordVisible && _isPassword ? true : false,
        style: TextStyle(color: Colors.white, fontFamily: 'Avenir Black'),
        decoration: InputDecoration(
            labelText: _title,
            labelStyle:
                TextStyle(fontFamily: 'Avenir Book', color: Colors.white),
            suffixIcon: _isPassword
                ? IconButton(
                    icon: isPasswordVisible
                        ? Icon(Icons.visibility_off, color: Colors.white)
                        : Icon(Icons.visibility, color: Colors.white),
                    onPressed: () {
                      setState(() {
                        isPasswordVisible = !isPasswordVisible;
                      });
                    })
                : SizedBox()),
      ),
    );
  }

  buildButton(String _title, Color _color, Color _txtColor, Function func) {
    return Container(
        width: 200,
        decoration: BoxDecoration(
            color: _color,
            borderRadius:
                BorderRadius.circular(15)),
        child: FlatButton(
          child: Text(
            _title,
            style: TextStyle(
                color: _txtColor,
                fontFamily: 'Avenir Black',
                fontSize: 12),
          ),
          onPressed: func,
        ));
  }

  //===================================================================================================================
  buildAll() {
    return SingleChildScrollView(
      child: FutureBuilder(
          future: isLoading,
          builder: (context, snapshot) {
            if (snapshot.data == true) return circularProgress();

            return Container(
                padding: EdgeInsetsDirectional.only(
                    start: 10,
                    end: 10),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 10),
                    //Heade Title
                    buildHeader(),
                    SizedBox(height: 10),

                    //Input Field
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if(!isLogin) buildInputField('Username', username, false),
                          buildInputField('Email', email, false),
                          buildInputField('Password', password, true),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                      buildButton(
                        isLogin?
                          'LOGIN' : 'DAFTAR',
                          Colors.white,
                          ColorList.primaryColor,
                          () => isLogin? login(email.text, password.text) : createUser(username.text, email.text, password.text)),
                      
                      SizedBox(height: 10),
                      buildButton(
                      isLogin?
                        'Belum punya akun?' : 'Sudah punya akun?',
                        ColorList.primaryColor,
                        Colors.white,
                        () => changeIsLogingStatus()),
                      ],
                    )
                  ],
                ));
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorList.primaryColor,
      body: Center(
        child: buildAll(),
      ),
    );
  }
}
