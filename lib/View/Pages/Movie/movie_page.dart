import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:test_majoo/Controller/Movie/movie_controller.dart';
import 'package:test_majoo/View/Template/color_list.dart';
import 'package:test_majoo/View/Template/progress.dart';
import 'package:test_majoo/View/Widgets/Buttons/rounded_button.dart';
import 'package:test_majoo/View/Widgets/Headline/heading_text.dart';

import 'package:carousel_slider/carousel_slider.dart';

class MoviePage extends StatefulWidget {

  final String movieID;

  MoviePage({
    this.movieID
  });

  @override
  _MoviePageState createState() => _MoviePageState();
}

class _MoviePageState extends StateMVC<MoviePage> {
  

  //Controller
   MovieController _controller;
  _MoviePageState(): super(MovieController())
  {
    _controller = MovieController.con;
  }

  //Status
  Future<bool> isLoading = Future<bool>.value(true);

  //Data
  int _index = 0;
  List<Container> imageList = [];

  @override
  void initState() {
    super.initState();
    setData();
  }

  setData() async{
    setLoadingValue(true);
    await _controller.getMovieDetails(widget.movieID, this.context);
    setImageSlide(_controller.moviePosters);
    setLoadingValue(false);
  }

  setLoadingValue(bool _val)
  {
    setState(() {
      isLoading = Future<bool>.value(_val);
    });
  }

  setImageSlide(List<String> img) {
    var size = MediaQuery.of(context).size;
    if (img != null) {
      for (var _img in img) {
        imageList.add(
          Container(
              width: size.width * .9,
              decoration: BoxDecoration(
                  image: DecorationImage(image: NetworkImage(_img)))),
        );
      }
    }
  }

  //===================================================================================
  //BUILDER
  buildCarouselContainer() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: ColorList.primaryColor,
              borderRadius: BorderRadius.circular(10)
            ),
            child: Header(title: 'Images related' , color: Colors.white, textSize: 12,)),
          SizedBox(height: 10),
          CarouselSlider(
              options: CarouselOptions(
                enableInfiniteScroll: false,
                height: 200,
                aspectRatio: 16 / 9,
                onPageChanged: (index, reason) {
                  setState(() {
                    print('Change index ' + index.toString());
                    _index = index;
                  });
                },
              ),
              items: imageList),
        ],
      ),
    );
  }


  buildItemImage(String _imgUrl){
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          height: 200,
          width:  170,
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(_imgUrl)
            )
          ),
        ),
        SizedBox(width: 10),

        buildHeader(_controller.movies.title != null? _controller.movies.title : '', _controller.movies.year != null? _controller.movies.year : '' ,ColorList.headingColor, ColorList.headingColor),
      ],
    );
  }

  buildHeader(String _itemName, String _itemType, Color titleColor, Color subTitleColor){
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 100,
            child: Header(title: _itemName, color: titleColor, textSize: 14)),
          SizedBox(height:5),
          Header(title: _itemType, color: titleColor, textSize: 12),
        ],
      ),
    );  
  }

  buildDescColumn(String _desc){
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .60,
      alignment: Alignment.center,
      child: ListView(
        children:
        [ 
          Header(title: 'Sinopsis', color: ColorList.headingColor, textSize: 14),
          SizedBox(height: 10),
          Text(_desc, style: TextStyle(fontSize: 12, color: ColorList.headingColor))
        ]
      )
    );
  }

  buildRowEditButton(){
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          RoundedButton(color: Colors.grey, title: 'Edit', title_color: Colors.white, func: ()=> null,),
          SizedBox(width: 10),
          RoundedButton(color: Colors.red, title: 'Hapus', title_color: Colors.white, func: ()=> null ,),
        ],
      ),
    );
  }

  buildAll(){
    return SingleChildScrollView(
      child: FutureBuilder(
        future: isLoading,
        builder: (context, snapshot) {
          if(snapshot.data == true)
            return circularProgress();

          return Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height:20),
                buildItemImage(_controller.movies.imageURl != null? _controller.movies.imageURl :''),
                SizedBox(height: 10),
                buildCarouselContainer(),
                SizedBox(height: 10),
                buildDescColumn(_controller.synopses != null? _controller.synopses : '')
              ],
            ),
          );
        }
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Film', style: TextStyle(color: Colors.white),), backgroundColor: ColorList.primaryColor, centerTitle: true, automaticallyImplyLeading: true,),
      body: buildAll(),
    );
  }
}