import 'package:flutter/material.dart';

class Header extends StatelessWidget {

  String title;
  final Color color;
  final double textSize;

  Header({
    this.title = '',
     this.color,
     this.textSize
  });

  buildAll(){
    return Text(title, style: TextStyle(fontWeight: FontWeight.w600, color: color, fontSize: textSize), overflow: TextOverflow.clip,);
  }

  @override
  Widget build(BuildContext context) {
    return buildAll();
  }
}

class SubHeader extends StatelessWidget {

  final String title;
  final Color color;
  final double textSize;

  SubHeader({
     this.title,
     this.color,
     this.textSize
  });

  buildAll(){
    return Text(title, style: TextStyle(fontWeight: FontWeight.w200, color: color, fontSize: textSize));
  }

  @override
  Widget build(BuildContext context) {
    return buildAll();
  }
}