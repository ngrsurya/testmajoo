import 'package:flutter/material.dart';
import 'package:test_majoo/View/Template/color_list.dart';

class MovieCard extends StatelessWidget {

  final String movieName;
  final String itemID;
  final String year;
  final String imgURL;
  final Function func;

  MovieCard({
     this.movieName,
     this.itemID,
     this.year,
     this.imgURL,
     this.func
  });

  buildAll(){
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.grey[200]
      ),
      child: Column(
        children: [
          GestureDetector(
            onTap: (){
              func();
            },
            child: Container(
              height: 200,
              width: 220,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only( topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(imgURL)
                )
              ),
            ),
          ),
          SizedBox(height: 10),

          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FittedBox(
                  fit: BoxFit.cover,
                  alignment: Alignment.centerLeft,
                  child: Text(movieName, style: TextStyle(fontWeight: FontWeight.w600, color: ColorList.headingColor),
                    overflow: TextOverflow.clip
                  ,),
                ),

                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(year,style: TextStyle(fontWeight: FontWeight.w400, color: ColorList.headingColor), ))
              ],
            )
          ),
        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return buildAll();
  }
}