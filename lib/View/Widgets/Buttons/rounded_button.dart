import 'package:flutter/material.dart';
import 'package:test_majoo/View/Widgets/Headline/heading_text.dart';

class RoundedButton extends StatelessWidget {

  final String title;
  final Color color;
  final Color title_color;
  final Function func;

  RoundedButton({
     this.title,
     this.color,
     this.title_color,
     this.func
  });

  buildAll(){
    return Container(
      alignment: Alignment.center,
      height: 40,
      width: 70,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: color
      ),
      child: FittedBox(
        fit: BoxFit.cover,
        child: FlatButton(
          onPressed: func(),
          child: SubHeader(title: title, color: title_color, textSize: 12),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildAll();
  }
}