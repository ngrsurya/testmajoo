
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:test_majoo/Controller/Connector/api.dart';
import 'package:test_majoo/Controller/Connector/http_blocker.dart';
import 'package:test_majoo/Controller/Connector/url_communicator.dart';
import 'package:test_majoo/Model/Movie.dart';
import 'package:test_majoo/View/Pages/Movie/movie_page.dart';

class HomeController extends ControllerMVC
{
  factory HomeController()
  {
    if(_this == null)
       _this = HomeController._();
    return _this;
  }

  //Singleton Factory
  static HomeController _this =  HomeController._();
  HomeController._();
  static HomeController get con => _this;

  //Api List
  String apiLink = Api.apiLink;
  String getMovieListLink = Api.getPopularMovieLink;
  String getMovieDetailLink = Api.getMoviesDetailLink;

  // //Setter
  List<Movie> get movies => _movies;

  // //Getter
  List<Movie> _movies = [];

  //FUNCTION
  getMovieLst(var _context) async{
    final param ={
      'homeCountry' : 'US',
      'purchaseCountry' : 'US',
      'currentCountry' : 'US'
    };

    var res = await urlRequest.getFunction(apiLink, getMovieListLink, false);
    bool statusCode = await HttpBlocker.checkStatusCode(res, _context);

    if(statusCode)
    {
      print('get movie list');

      dynamic resBody = json.decode(res.body);
      List<String> idList = [];
      print(resBody);

      for(int i = 0; i < 10; i++){
        String txtHolder = resBody[i];
        txtHolder = txtHolder.replaceAll("/title/", "");
        txtHolder = txtHolder.replaceAll("/", "");

        print('title ID ' + txtHolder);
        idList.add(txtHolder);
      }

      await getMovieDetails(idList, _context);
      
    }else{
      print('fail get device list');
    }
  }

  Future<void> getMovieDetails(List<String> _idMovies, var _context) async{
    print('get movie detail');

    for(var item in _idMovies)
    {
      var param ={
        "tconst": item
      };
      var res = await urlRequest.getFunction(apiLink, getMovieDetailLink, true, param: param);
      print(res.body);
      bool statusCode = await HttpBlocker.checkStatusCode(res, _context);

      if(statusCode)
      {
        print('succed movie detail');

        var resBody = json.decode(res.body);
        Movie movHolder = Movie.fromJson(resBody, item);
        
        if(movHolder != null) _movies.add(movHolder);
        
      }
    }
    print('Movie lenght ' + _movies.length.toString());
  }


  goDetailMoviePage(var _context, String _movieID){
    setState(() {
        Navigator.push(
          _context,
          MaterialPageRoute(
              builder: (context) => MoviePage(
                movieID: _movieID,
              )));
    });
  }
  
}