
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:test_majoo/Controller/Connector/api.dart';
import 'package:test_majoo/Controller/Connector/http_blocker.dart';
import 'package:test_majoo/Controller/Connector/url_communicator.dart';
import 'package:test_majoo/Model/Movie.dart';
import 'package:test_majoo/Model/movie_image.dart';
import 'package:test_majoo/Model/movie_synopses.dart';
import 'package:test_majoo/View/Pages/Movie/movie_page.dart';

class MovieController extends ControllerMVC
{
  factory MovieController()
  {
    if(_this == null)
       _this = MovieController._();
    return _this;
  }

  //Singleton Factory
  static MovieController _this =  MovieController._();
  MovieController._();
  static MovieController get con => _this;

  //Api List
  String apiLink = Api.apiLink;
  String getMovieListLink = Api.getPopularMovieLink;
  String getMovieDetailLink = Api.getMoviesDetailLink;  
  String getMociePosterLink = Api.getMoviePostersLink;
  String getMovieSynopsesLink = Api.getMovieSynopsesLink;

  // //Setter
  Movie get movies => _movies;
  List<String> get moviePosters => _moviePosters;
  String get synopses => _synopses;

  // //Getter
  Movie _movies;
  List<String> _moviePosters = [];
  String _synopses = '';

  //FUNCTION
  Future<void> getMovieDetails(String _idMovies, var _context) async{
    print('get movie detail');

    var param ={
      "tconst": _idMovies
    };
    var res = await urlRequest.getFunction(apiLink, getMovieDetailLink, true, param: param);
    print(res.body);
    bool statusCode = await HttpBlocker.checkStatusCode(res, _context);

    if(statusCode)
    {
      print('succed movie detail');

      var resBody = json.decode(res.body);
      Movie movHolder = Movie.fromJson(resBody, _idMovies);
      
      if(movHolder != null) _movies = movHolder;
      await getMoviePosters(_idMovies, _context);   //Get movie posters
      await getMovieSynopsise(_idMovies, _context); //Get movie synposes
      
    }
  }

  Future<void> getMoviePosters (String _idMovies, var _context) async{
    print('get movie detail');

    var param ={
      "tconst": _idMovies
    };
    var res = await urlRequest.getFunction(apiLink, getMociePosterLink, true, param: param);
    print(res.body);
    bool statusCode = await HttpBlocker.checkStatusCode(res, _context);

    if(statusCode)
    {
      print('succed movie detail');

      var resBody = json.decode(res.body);
      var data = resBody['images'];
      List<String> urlHolder = [];

      for(int i = 0; i < 5; i++)
      {
        MoviePoster posterHolder = MoviePoster.fromJson(data[i]);
        if(posterHolder != null) urlHolder.add(posterHolder.imgUrl);
      }
      
      _moviePosters = urlHolder;
    }
  }

  Future<void> getMovieSynopsise (String _idMovies, var _context) async{
    print('get movie detail');

    var param ={
      "tconst": _idMovies
    };
    var res = await urlRequest.getFunction(apiLink, getMovieSynopsesLink, true, param: param);
    print(res.body);
    bool statusCode = await HttpBlocker.checkStatusCode(res, _context);

    if(statusCode)
    {
      print('succed movie detail');
      var resBody = json.decode(res.body);

      try
      {
        var data = resBody[0];
        MovieSynopses val = data != null?  MovieSynopses.fromJson(data) : null;      
        _synopses = val.synopses;

      }catch(e){
        _synopses = '';

      }
    }
  }


  goDetailMoviePage(var _context, String _movieID){
    setState(() {
        Navigator.push(
          _context,
          MaterialPageRoute(
              builder: (context) => MoviePage(
                movieID: _movieID,
              )));
    });
  }
  
}