import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:flutter/material.dart';

class HttpBlocker {
  static bool getTheStatus(var json) {
    bool isValid = false;
    isValid = json['status'];
    print('status ' + isValid.toString());

    return isValid;
  }

  static Future<bool> checkStatusCode(
    http.Response response, var context) async {
    int statusCode = 0;
    bool statusAcceptable = false;

    //LogOut Variable
    bool isLogout = false;

    statusCode = response.statusCode;
    print(response.body);
    print('res status code ' + statusCode.toString());

    //Switch condition of Status Code
    if (statusCode != null) {
      switch (statusCode) {
        case 200:
          statusAcceptable = true;
          break;
        case 201:
          statusAcceptable = true;
          break;
        case 401:
          //isLogout = await logOut(token, context);
          break;
        default:
      }
    }

    if (!isLogout) {
      return statusAcceptable;
    } else {
      print('LogOut');
      //goToRootPage(isLogout, context);
    }
  }

  //Path location
  //Path
  // static Future<String> get _localPath async {
  //   print('Set local path');
  //   final directory = await getApplicationDocumentsDirectory();

  //   return directory.path;
  // }

  // static Future<File> get _localFile async {
  //   final path = await _localPath;
  //   return File('$path/token.txt');
  // }

  // static writeFile() async {
  //   print('token saved local');
  //   var prefs = await SharedPreferences.getInstance();
  //   prefs.setString('kodeToken', '');
  // }

  //Back to home page
  // static goToRootPage(bool val, context) {
  //   Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => MainPage()));
  // }
}
