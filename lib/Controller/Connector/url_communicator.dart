import 'package:http/http.dart' as http;
import 'dart:io';

import 'package:test_majoo/Controller/Connector/api.dart';

class urlRequest {

  //Post Method
  static Future<http.Response> loginFunction(
      String api, String link) async {
    var res;
    try {
      res = await http.post(
          Uri.https(
            api,
            link,
          ),
          headers: Api.headers
      );
    } catch (e) {
      res = null;
    }

    return res;
  }

  //Post Method
  static Future<http.Response> postFunction(
      String api, String link, bool useParam,
      {var param}) async {
    var res;
    try {
      if (useParam)
        res = await http.post(Uri.https(api, link, param), headers:Api.headers);
      else
        res = await http.post(Uri.https(api, link), headers: Api.headers);
    } catch (e) {
      res = null;
    }

    return res;
  }

  //Get Method
  static Future<http.Response> getFunction(
      String api, String link , bool usingParam,
      {var param}) async {
    print(link);
    print(api + link);
    var res;
    try {
      if(usingParam)
      {
        res = await http.get(
          Uri.https(api, link, param ),
          headers: Api.headers
        );
      }
      else
      {
        res = await http.get(
          Uri.https(api, link),
          headers: Api.headers
        );
      }
    } catch (e) {
      res = null;
    }

    print('getting res');
    print(res);
    return res;
  }

  //Put Method
  static Future<http.Response> putFunction(
      String api, String link, bool usingParam, var body,
      {var param}) async {
    var res;
    try {
      res = await http.put(Uri.https(api, link, usingParam ? param : null),
          headers: Api.headers,
          body: body
      );
    } catch (e) {
      print(e);
      res = null;
    }

    return res;
  }
}
