class Api{

  static final Map<String, String> headers = {
    "x-rapidapi-host": "imdb8.p.rapidapi.com",
    "x-rapidapi-key": "25cbb4a78dmsh253e2fb959e52dep124986jsn62bd3835faf5"
  };

  //Link
  static final String apiLink = 'imdb8.p.rapidapi.com';

  static final String getPopularMovieLink = '/title/get-most-popular-movies';
  static final String getMoviesDetailLink = '/title/get-details';
  static final String getMoviePostersLink = '/title/get-images';
  static final String getMovieSynopsesLink = 'title/get-synopses';
}