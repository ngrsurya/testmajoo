import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:test_majoo/Model/User/user.dart';
import 'package:test_majoo/View/Pages/Dashboard/home_page.dart';
import 'package:test_majoo/db/user_database.dart';


class UserController extends ControllerMVC {
  //Singleton Factory
  factory UserController() {
    if (_this == null) _this = UserController._();
    return _this;
  }
  static UserController _this;
  UserController._();
  static UserController get con => _this;



  //FUNCTION
  Future<bool> loginUser(var _context, String _email, String _password) async{
    dynamic res = await UserDatabase.instance.loginUser(_email, _password);
    
    if(res == true)
    {
      goHomePage(_context);
    }
    return res;
  }

  Future<bool> createUser(var _context, String _email, String _username, String _password) async{
    User userHold = User(email: _email, password: _password, username: _username);
    dynamic res = await UserDatabase.instance.createUser(userHold);
    print('berhasil ga' + res.toString());
    return res; 
  }

  goHomePage(var _context){
    setState(() {
        Navigator.push(
          _context,
          MaterialPageRoute(
              builder: (context) => HomePage(
              )));
    });
  }
}
