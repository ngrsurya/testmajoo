import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:test_majoo/Model/User/user.dart';

class UserDatabase{
  static final UserDatabase instance = UserDatabase._init();
  static Database _database;
  UserDatabase._init();

  Future<Database> get database async{
    if(_database != null) return _database;

    _database = await _initDB('user.db');
    return _database;
  }

  Future<Database> _initDB(String filepath) async{
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filepath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async{
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final textType = 'TEXT NOT NULL';
    await db.execute(
      '''
        CREATE TABLE $tableUser(
          ${UserField.id} $idType,
          ${UserField.username} $textType,
          ${UserField.password} $textType,
          ${UserField.email} $textType
        )
      '''
    );
  }

  Future<bool> createUser(User product) async{
    final db = await instance.database;
    final id = await db.insert(tableUser, product.toJson());

    bool valToReturn = id != null? true :false;
    return valToReturn;
  }

  Future<bool> loginUser(String email, String password) async{
    final db = await instance.database;
    bool valToRetunr = false;
    final maps = await db.query(
      tableUser,
      columns: UserField.values,
      where: '${UserField.email} = ?',
      whereArgs: [email]
    );

    print(email);
    print(maps);

    if(maps.isNotEmpty){
      print('email ada');
      valToRetunr = await syncPass(password);
    }

    return valToRetunr;
  }


  Future<bool> syncPass(String password) async{
    final db = await instance.database;

    final maps = await db.query(
      tableUser,
      columns: UserField.values,
      where: '${UserField.password} = ?',
      whereArgs: [password]
    );

    if(maps.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  Future close() async{
    final db = await instance.database;
    db.close();
  }
  
}