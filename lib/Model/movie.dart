class Movie{
  
  final String id;
  final String title;
  final String year;
  final String imageURl;

  Movie({
    this.id,
    this.title,
    this.year,
    this.imageURl
  });

  factory Movie.fromJson(Map<String, dynamic> json, String _id){
    return new Movie(
      id: _id,
      title: json['title'],
      year: json['year'].toString(),
      imageURl: json['image']['url']
    );
  }
}