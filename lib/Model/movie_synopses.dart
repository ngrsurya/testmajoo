class MovieSynopses{
  
  final String synopses;

  MovieSynopses({
    this.synopses,
  });

  factory MovieSynopses.fromJson(var json){
    return new MovieSynopses(
      synopses: json['text'] != null? json['text'] : ''
    );
  }
}