import 'dart:io';

final String tableUser = 'user';

class UserField{

  static final List<String> values = [
    id, username, email, password
  ];

  static final String id = 'id';
  static final String username = 'username';
  static final String email = 'email';
  static const String password = 'password';
}

class User{
  
  final String id;
  final String username;
  final String password;
  final String email;

  const User({
    this.id,
    this.username,
    this.password,
    this.email
  });

  User copy({
    String id,
    String username,
    String password,
    String email
  })=> User(
    id: id?? this.id,
    username: username?? this.username,
    password: password?? this.password,
    email: email?? this.email,
  );

  static User fromJson(Map<String, dynamic> json){
    return new User(
      id: json[UserField.id],
      username: json[UserField.username],
      password: json[UserField.password] as String, 
      email: json[UserField.email] as String);
  }
  
  Map<String, Object> toJson() => {
    UserField.id : id,
    UserField.username: username,
    UserField.password: password,
    UserField.email: email,
  };
}