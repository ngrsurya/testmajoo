class MoviePoster{
  
  final String imgUrl;

  MoviePoster({
    this.imgUrl,
  });

  factory MoviePoster.fromJson(Map<String, dynamic> json){
    return new MoviePoster(
      imgUrl: json['url']
    );
  }
}